//MiniX5 
//Made by: Maj Tofting 

let x = 0;
let y = 0;
let spacing = 20;
let color1, color2, color3;
let startPoint;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(226, 213, 196);
  frameRate(15);
  //text: Pink Floyd reference
  textSize(24);
  textFont('Georgia')
  textAlign(CENTER);
  text("All in all it's just...", width/4, height/3);
  text("... another brick in the wall", width-(width/4), height-height/4);
}

function draw() {
  drawBricks();
  drawGrass();
}

function drawBricks(){
  stroke(195, 183, 167);
  strokeWeight(3);
  color1 = color(140, 67, 45);
  color2 = color(177, 99, 63);
  color3 = color(87, 72, 71);
  if (random(4) > 2) {
    //brick color1
    fill(color1);
    rect(x, y, spacing*4, spacing);
  } else if (random(4) > 1) {
    //brick color2
    fill(color2);
    rect(x, y, spacing*4, spacing);
  } else if (random(4) > 0) {
    //brick color3
    fill(color3);
    rect(x, y, spacing*4, spacing);
  }
  x+=spacing*4; //draw new brick at end of brick prior brick
  if (x >= width) { //if bricks have filled out window vertically
    startPoint =  x - width //define starting point for bricks
     startPoint -- //decrement starting point
    x = 0 - startPoint*random(2, 3); //create random starting point for next line of bricks
    y += spacing;
  }
  if (y > height) { //if bricks have filled out window horisontally --> reset x and y
    x = 0;
    y = 0;
  }
}

function drawGrass() {
  //ground
  noStroke();
  let grass = color(96, 196, 82);
  fill(grass);
  rect(0, height-10, width, 10);
  //grass
  stroke(grass);
  strokeWeight(3);
  for (i = 0; i < width; i = i+5) {
    line(i, height-(random(20)), i, height);
  }
  if (y > spacing*2) {
    for (i = 0; i < width; i = i+5) {
      line(i, height-20-(random(20)), i, height-20);
    }
  }
  if (y > spacing*4) {
    for (i = 0; i < width; i = i+5) {
      line(i, height-40-(random(20)), i, height-40);
    }
  }
  if (y > spacing*6) {
    for (i = 0; i < width; i = i+5) {
      line(i, height-60-(random(20)), i, height-60);
    }
  }
  if (y > spacing*8) {
    for (i = 0; i < width; i = i+5) {
      line(i, height-80-(random(20)), i, height-80);
    }
  }
}
