## MiniX 5 Submission

Project this week: Auto Generator

> "Randomness and chance operations are so necessary to daily life, well beyond the realm of games, that randomness itself is framed as fixed, repeatable, and knowable." - Montfort et al. in *"Randomness", 10 PRINT CHR$(205.5+RND(1)); : GOTO 10*

#### How to see my program

1.  [RUN](https://majtofting.gitlab.io/aestheticprogramming/miniX5/) my program

2.  [READ](https://gitlab.com/majtofting/aestheticprogramming/-/blob/main/miniX5/bricks.js) the code

#### About the program
This miniX is inspired by 10 PRINT as it mimics the placement of bricks to create a something that looks like a brick wall. At the 'ground' green shapes simulate grass that grows to a certain height. In the background I put lyrics from a Pink Floyd song (*'[Another Brick in the Wall](https://www.youtube.com/watch?v=YR5ApYxkU-U)'*) and with time the bricks cover the text.

<img src = 'bricks_1.png' width = 600>


<img src = 'bricks_2.png' width = 600>


##### 1. The rules in my program and how it performs over time
The program follows several rules for it to function but the overall idea it based on two main rules.
These are:
  1. Create bricks with a random color (of three options) and place brick rows staggered

  2. Create grass straws and let each grow with a random length to a max height

Over time the production of bricks fill out the canvas with rows of bricks covering up the text behind it. I programmed it so that the rows would be located on top of each other in a staggered (and to some degree) random fashion. The grass "grows" in five leaps and between these leaps the straws grow with different length (to mimic different speeds). I meant to make each straw grow with a random speed in *one* smooth leap, but due to the relatively high `frameRate` all the straws quickly reached the max height, making it difficult to even notice the different speeds/lengths.

##### 2. The role of the rules and processes
In my program the role of the two main rules were mainly to set the framework for the program. When trying to execute the principles I had to create many smaller additional 'rules' to get the program to functions properly. To create the brick wall I used the **conditional statements** `if` and `else if`  in combination with `random` to make the program chose between the different brick colors. The conditional statement `if` was also used to 'move' the location of new bricks so that the bricks were drawn next to and below the prior bricks.

To create the grass straws I used a **for-loop** to draw many straws next to each other along the entire width. Because I ended up making the straws grow in five leaps I also had to create five for-loops and combining four of these with an `if` statement. Additionally I used `random` to make each straw grow with a random length between the leaps (this effect isn't that evident due to the high `frameRate`).

Overall the role of the rules is to predeterminate what will happen in the program. Be using functions as `random` some of the outcomes will be unexpected while other part will be as you planned. I would argue that my program is relatively limited in the ability to create surprising outcomes, and this is due to the rules I have created. 

##### 3. How my miniX helps to understand the idea of "auto-generator"
I find that many of the scholars we have read and talked about this week implicitly attributes the computer more autonomy than I think it has. Initially I wanted to try and prove myself wrong by creating a program with simple rules that would give the computer so much autonomy that I myself would be surprised by the outcome. I soon realised that in order to make the computer opperate 'randomly' you have explicitly use functions like `random` to make it do se. For instance, if I want to create a program that 'auto-generates' a lot of squares, the only rules being a fixed size and that the squares will be drawn within the canvas, I explicitly need to 'tell' the computer to make everything else (position, fill and stroke color, stroke weight etc.) random by using the `random` command. 

Therefore I ended up moving away from my original idea to create a program that - in my opinion - is fairly predictable as I difined the framework. Also, the program simulated bricks in slighty different colors which a real life example on how randomness has become such a intergrated part of our lives "that randomness itself is framed as fixed, repeatable, and knowable" (2). The outcome of my "random" program therefore appears very familiar. 

##### References
1. Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142
2. Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.
