## MiniX 3 Submission

Project this week: Throbber

> "Technology has been a tool through which humans create distance from natural cycles and design their own time experiences." (Lamerant, 2018)

#### How to see my throbber

1.  [RUN](https://majtofting.gitlab.io/aestheticprogramming/miniX3/) my program

2.  [READ](https://gitlab.com/majtofting/aestheticprogramming/-/blob/main/miniX3/Throbber.js) the code

#### About the program

###### 1. What have I created?
I have created a throbber that illustrates peace signs rotating in a circle. These sign are made with ellipses and lines. The background is yellow and on top of it I have made a blue rectangle to create a Ukrainian flag. I used different time-related function to make the throbber look animated.

![](throbber.mp4)

##### 2. What do I want to explore/express?
**On a practical level** I wanted to explore how to even create a program that would make the throbber look animated. My first challenge was to make the elements appear in a circle. I figured that I would need to use the rotate function. I then found inspiration from the example from [AestheticProgramming](https://aesthetic-programming.net/pages/3-infinite-loops.html) to make use of the lines let `cir = 360/num*(frameCount); rotate(radians(cir));`. This made the elements appear one by one if I placed the background in setup.

My next challenge was to make the drawing reset when all the peace sign had been drawn. To fix this a first tried to create the circle with a for-loop but this just made all the element appear at once. I then returned to the original sketch but I still couldn't find a way to make the drawing reset.
Therefore I ended up placing the background in draw also as well as change the alpha value. By doing this I got a result closer to what I wanted.

**On a higher level** I wanted to use the throbber to pay attention to a current politic conflict: the war in Ukraine. The overall intention with this choice is to use the throbber as a medium to make the user reflect about a specific event or message. The reason for this is twofold: 1) a throbber is not connected to the processing of the computer and thereby does not give any reliable indication of how much time is left or how fast the computer is working.

>"We consider this (a throbber, Ed.) an evocative symbol as it illuminates the discrepancy between what we think we know and what we don’t know about the hidden machine labor, and the complexity of multiple temporalities that run during any given computational operation" (Soon & Cox, 2020)


Therefore I find silly to create a throbber which only purpose is to create this illusion of the computer working.
2) because a throbber is equal to waiting - but doesn't in itself have any functional purpose - I find that this time might as well be spent reflecting upon current events or similar.

**On a more metaphorical level** I find the symbolism between a throbber and the situation in Ukraine to be fitting; for everyone who isn't a part of the decision making - who doesn't have knowledge about the inner workings of the operations - witnessing the war becomes a question of waiting. Just like with a throbber, we know something is waiting on the other side - we just don't for how long the throbber will run, or which page will be loaded.

##### 3. What are time-related syntaxes/functions that I have used?
- I have used `rotate` to make rotate my peace signs so that it looks like is going around in a circle. By using this function I didn't need to hard code every single element I spend time calculating their individual position.
- I used `translate` to make place the center of the circle in the middle of the window/canvas.
- I have used `frameRate` to set the speed so that the throbber runs slower than the default 60 FPS.
- I have used `frameCount` to make the element change position from frame to frame.
- I used `push()` and `pop()` to isolate the rotate and translate functions, so they only would affect the element within the borders of push and pop.

##### 4. A throbber I have encountered and what a throbber communicates and/or hides
A classical throbber that encounter several times a day - including every time I open a new tab on Google Chrome - is this one:  

![Gif of a throbber](miniX3/throbber_12.gif) 

It is subtle but it is there every time I open a new tab or re-load a page.
A throbber communicates that the computer is loading something. It is a symbol to tell us that something is happening but it just takes time.
On one hand the throbber is a good way to tells us that something is happening - it can even be beneficial when the throbber tells the status for saving something so that you don't close a page or program to soon.

On the other hand you could argue that the throbber is contributing to the gap that exist between many computer user and the actual processes within the computer. Essentially the throbber hides the actual operations of the computer while still informing us that our desired content is on the way.
We could question whether it is desirable or not have this discrepancy between what the computer is doing and what we think it is doing. As I see it there are argument both for and against.

##### References
1. Hans Lammerant, “How humans and machines negotiate experience of time,” in [The Techno-Galactic Guide to Software Observation, 88-98, (2018)](https://monoskop.org/log/?p=20190)
2. Soon & Cox, "Infinite Loops", in [AestheticProgramming (2020)](https://aesthetic-programming.net/pages/3-infinite-loops.html)
