//MiniX3 - Throbber
//Course: Aesthetic Programming, Aarhus University
//gitlap:

function setup(){
  //create a full window canvas
  createCanvas(windowWidth, windowHeight);
  background(241, 212, 5); //background color yellow
  fill(11, 75, 198); //color blue
  noStroke();
  rect(0, 0, windowWidth, windowHeight/2); //rect that fills half of window
  frameRate(5); //set speed of the throbber
}

function draw(){
  /*placing the background in setup ensures that the background will be
  in full color from first frame;
  placing it in draw and changing the alpha value makes the trobber
  look animated, as a 70% saturated yellow background is draw for every frame*/
  background(241, 212, 5, 60); //yellow background - this time with alpha
  /*placing the blue rect in draw so it won't be drawn over by the background*/
  fill(11, 75, 198); //color blue
  noStroke();
  rect(0, 0, windowWidth, windowHeight/2); //blue rect again
  drawPeace2(); //draw throbber

}

/*I made two versions of the throbber and chose the second one;
this function is not run in draw*/
function drawPeace1(){
  let num = 10; //number of peace signs
  let size = 30; //make variable for size
  let x = 60; //make variable for x-position
  push();
    translate(width/2-100, height/2+100); //move origin to center
    /* 360/num >> calculate degrees of which peace sign's movement;
    frameCount%num >> the remainder tells which position is next */
    let cir = 360/num*(frameCount%num);
    rotate(radians(cir)); //make the elements rotate around origin
    noFill();
    stroke(47, 166, 35); //color of stroke
    strokeWeight(4);
    ellipse(x, 0, size, size); //create cirkles
    line(x+size/2, 0, x-size/2, 0); //create line that goes through center
    line(x+size/4, size/3, x, 0); //create line from middle to right side
    line(x+size/4, -size/3, x, 0); //create line from middle to left side
  pop();

}

function drawPeace2(){
  let num = 10; //number of peace signs
  let size = 30; //make variable for size
  let x = 60; //make variable for x-position
  push();
    /*move origin to center vertically but slightly under center horisontally
    because the effect with alpha value only work over the background*/
    translate(width/2, height/2+100);
    /* 360/num >> calculate degrees of which peace sign's movement;
    frameCount%num >> the remainder knows which position is next */
    let cir = 360/num*(frameCount*0.5);
    rotate(radians(cir)); //make the elements rotate around origin
    fill(241, 212, 5); //same color as background
    stroke(0); //color of stroke
    strokeWeight(4);
    ellipse(x, 0, size, size); //create cirkles
    line(x+size/2, 0, x-size/2, 0); //create line that goes through center
    line(x+size/4, size/3, x, 0); //create line from middle to right side
    line(x+size/4, -size/3, x, 0); //create line from middle to left side
  pop();
}
