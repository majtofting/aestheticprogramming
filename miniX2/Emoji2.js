//Variables for bezier
let p1 = { x: 140, y: 240};
let p2 = { x: 180, y: 280};
let p3 = { x: 220, y: 280};
let p4 = { x: 260, y: 240};
//Variable for lenght of triangles
let s = 315;

function setup() {
createCanvas(400, 400);
}

function draw() {
//Shirt color
let c = color(246, 249, 147);
//Shirt stroke color
let st = color(204, 207, 101);
//Mouse constraints
let yc = constrain(mouseY, 190, 290);

background(155, 242, 213);
//Text with instructions in top right corner
textSize(14);
fill(100, 188, 155);
noStroke();
text('Change mouth: Up and down', 10, 20);
text('Change eyes: Left and right', 10, 40);
//Body of emoji
  //rectMode(CENTER);
  //fill(c);
  //stroke(st);
  //strokeWeight(1);
  //line(175, 250, 175, 350);
  //line(225, 250, 225, 350);
//Neck of emoji
  //rect(200, 370, 220, 110, 30);
  //fill(c);
  //noStroke();
  //rect(200, 320, 50, 70);
  //stroke(st);
  //line(135, 370, 135, 400);
  //line(265, 370, 265, 400);
//Dents on the buttom of bag
fill(225, 209, 172);
stroke(170, 149, 111);
strokeWeight(1)
triangle(105, 300, 130, 300, 117, s);
triangle(130, 300, 155, 300, 143, s);
triangle(155, 300, 185, 300, 170, s);
triangle(185, 300, 210, 300, 197, s);
triangle(210, 300, 240, 300, 225, s);
triangle(240, 300, 265, 300, 255, s);
triangle(265, 300, 295, 300, 280, s);
//Shadow on top of bag
fill(189, 165, 123);
quad(105, 100, 125, 85, 275, 85, 295, 100);
//Front of paper bag
noStroke();
fill(225, 209, 172);
rectMode(CENTER);
rect(200, 201, 190, 200);
stroke(170, 149, 111);
line(105, 100, 105, 300);
line(295, 100, 295, 300);
line(105, 100, 295, 100);
//Marks in the top corners of bag
strokeWeight(1.5);
line(106, 100, 125, 130);
line(106, 107, 118, 125);
line(106, 114, 113, 125);
line(294, 100, 275, 130);
line(294, 107, 283, 125);
line(294, 114, 288, 125);
//Mouth (Variable 'yc' makes it follow mouseY)
noFill();
stroke(0);
strokeWeight(1)
bezier(p1.x, p1.y, p2.x, yc, p3.x, yc, p4.x, p4.y);
//Using conditional statements to shift between eyes
if (mouseX < 100){
//Draws eyes 1.0
fill(255);
stroke('grey');
ellipse(180,175, 30, 50);
ellipse(220, 175, 30, 50);
fill(0);
noStroke();
ellipse(175, 175, 20, 25);
ellipse(215, 175, 20, 25);
fill(255);
circle(180, 170, 10);
circle(220, 170, 10);
}
if (mouseX > 300) {
//Draws eyes 4.0
fill(246, 31, 28);
stroke(177, 25, 22);
strokeWeight(2);
//Uses defined function to make hearts (see below)
heart(175, 165, 30);
heart(225, 165, 30);
}
else if (mouseX > 200) {
 //Draws eyes 3.0
fill(0);
noStroke();
ellipse(180, 180, 15, 20);
ellipse(220, 180, 15, 20);
stroke(0);
strokeWeight(6);
strokeCap(ROUND);
line(165, 165, 190, 175);
line(210, 175, 235, 165);
}
else if (mouseX > 100){
//Draws eyes 2.0
fill(0);
circle(180, 190, 30);
circle(220, 190, 30);
//White in eyes
fill(255);
circle(177, 187, 18);
circle(217, 187, 18);
circle(187, 197, 10);
circle(227, 197, 10);
}
//Funcion to make heart shapes
function heart(x, y, size) {
beginShape();
vertex(x, y);
bezierVertex(x - size / 2, y - size / 2, x - size, y + size / 3, x, y + size);
bezierVertex(x + size, y + size / 3, x + size / 2, y - size / 2, x, y);
endShape(CLOSE);
}
}
