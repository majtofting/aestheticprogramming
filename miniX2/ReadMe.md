## MiniX2 Submission

Project this week: Emojis 💩🌈🍉

#### Options to access my Emoji
- Run the [program](https://majtofting.gitlab.io/aestheticprogramming/miniX2/)

- Read the [code](https://gitlab.com/majtofting/aestheticprogramming/-/blob/main/miniX2/Emoji2.js)

#### Description of my program

My program is runs an interactive Emoji that allows users to change between four different eyes by moving the mouse from left to right. Furthermore you can adjust the curve of the mouth by moving the mouse up and down. This allows for variation of several different facial expressions.

![Video of emoji](Emoji.mp4)

##### What I used
To make the program I experienced with a lot of different geometric shapes; such as rectangles, circles, ellipses, triangles, quads as well as straight lines and beziers. In the writing of these shapes I also tried to implement some variables to make some elements easier to change later on. An example of this is the length of the dents at bottom of the bag. I made the dent with seven individual triangles so in order to be able to change the length of all seven at once I used a variable.

To make the program interactive I made a variable for mouseY with some constraints so that the bezier (mouth) would follow the mouse horizontally within a limited area. For the eyes I used conditional statements in the form of 'if' and 'if else'. I chose to divide the canvas into four intervals of 100 pixel on the x-axis and assign each area to a different set of the four eye options. 

In the source code there are also sections of disabled code as I chose to make some changes late in the process. More on this in later. 

##### Inspiration
The aesthetics of the emoji is inspired by the work [*Multi*](https://www.o-r-g.com/apps/multi) by David Reinfurt. His work demonstrates how little is needed to make something that can be interpreted as facial expression. I therefore chose to focus on the essential elements of eyes and mouth and let the 'face' be a very neutral (almost boring) canvas. The face imitates a paper bag that someone/something has put over its head - a phenomenon you can find many pictures of on the internet: both from cartoons and real life.

##### Borrowed code
To make the hearts I found a [recipe to a basic heart shape](https://editor.p5js.org/Mithru/sketches/Hk1N1mMQg) made by a p5.js user called Mithru. I copied the last part of the program to use make heart shaped eyes for my Emoji. This saved my some time and furthermore I ended up with hearts that are a lot prettier than the ones in my previous miniX.

#### Reflection about wider context
In the text for this week we have read about some of the possible problems with Unicode and standardized Emojis. I think the following quotes from [Aesthetic Programming](https://aesthetic-programming.net/pages/2-variable-geometry.html) explains the situation quite well:

  >"Yet, as the standard expanded from the underlying characters and glyphs to symbol sets and emojis, the universalism has become increasingly problematic. Criticism has unsurprisingly centered on the politics of representation, such as blatant gender stereotyping and racial discrimination."

  >(...) "Our point is that using emojis may be fun and expressive, but they also tend to oversimplify and universalize differences, thereby perpetuating normative ideologies within already “violent power structures,” such that only selected people, those with specific skin tones for instance, are represented while others are not."

  My choices regarding the looks of the Emoji has been very influenced by the criticism in the text above. My intention was to create an Emoji that could represent as many people as possible. This turned out to be a very challenging task.

  *How do you represent every human being in one single visual when there are over 7,9 billion unique individuals in the world?*  

  After many attempts to come up with a truly universal Emoji I came to the conclusion that the more features I would add to my Emoji the more excluding it would become. If I chose a skin/hair/eye color it wouldn't represent all the people with a different color of skin/hair/eyes. The same was the case every other feature that would look too much like a specific characteristic a real person could have.

  ##### Thoughts about the final Emoji
  The result of the struggle to be inclusive became a paper bag with eyes and a mouth. By choice, these features looks animated and only serves to show emotions - not mimic realistic facial features. In the beginning I did make the neck and neckline a different color to imitate skin but then I realized that that meant choosing a color as 'skin color'. Therefore I changed it to being the same color as the rest for the shirt.

You could argue that I could just as well have chosen any non human like shape as the face but I think the cool thing about the paper bag is that it allows the viewer to imagine or decide that the face underneath it should look like. On the other hand, putting a bag over your head could also be interpretated as a sign of shame and hiding. The Emoji could therefore also be used to open a discussion about how far we should go in pursuit of inclusion. 

Lastly it shall be mentioned that I initially made a 'body' for the paper bag head. By doing this I also had to choice a certain body size which I later realized might be excluding to people of different sizes. To solve this problem I chose to let the paper bag float alone in the air just like many other disembodied Emojis.

See picture of first edition (with body) [here](https://gitlab.com/majtofting/aestheticprogramming/-/blob/main/miniX2/emoji-with-body.png). 

##### References
1. [Work: Multi](https://www.o-r-g.com/apps/multi) by David Reinfurt
2. [Source code: Hearts](https://editor.p5js.org/Mithru/sketches/Hk1N1mMQg) by Mithru
3.  [Aesthetic Programming](https://aesthetic-programming.net/pages/2-variable-geometry.html) (Online book)
4. The [p5.js reference](https://p5js.org/reference/) page
