function setup() {
  //rainbow background
  createCanvas(400,400);
  noStroke();
  colorMode(HSB, 400);
    for (let i = 0; i < 400; i++) {
      for (let j = 0; j < 400; j++) {
        stroke(i, j, 400);
        point(i, j);
      }
    }
  }
function draw() {
  noStroke();
  if (mouseIsPressed === true) {
    //text
    textSize(28);
    fill('pink')
    text('Keep the Love Alive', 70, 200);
    //big heart
    fill('red');
    ellipse(110,300,60,60);
    ellipse(155,300,60,60);
    triangle(82,313,183,313,132.5,365);
    //small heart
    fill('orange');
    ellipse(270,70,40,40);
    ellipse(300,70,40,40);
    triangle(252,80,318,80,285,108);
  } else {
    //text
    textSize(28);
    fill('black')
    text('Keep the Love Alive', 70, 200);
    //big heart
    fill('black');
    ellipse(110,300,60,60);
    ellipse(155,300,60,60);
    triangle(82,313,183,313,132.5,365);
    //small heart
    ellipse(270,70,40,40);
    ellipse(300,70,40,40);
    triangle(252,80,318,80,285,108);
  }

}
