## MiniX 1 submission

##### [Try my MiniX1 here](https://majtofting.gitlab.io/aestheticprogramming/miniX1/) 
##### [See how I made it here](https://gitlab.com/majtofting/aestheticprogramming/-/blob/main/miniX1/sketch.js)

### Answering questions 

#### What have I produced? 

I have produced a interactive GO-CARD like the ones you can find and take for free around in Danish cafes and cinemas. 


![Default version](miniX1-screenshot-1.png)
![When mouse is pressed](miniX1-screenshot-2.png) 

##### The what
On the pratical level the card consists of a **rainbow background** that I made mainly by changing the colorMode from RGB to HSB - [see how here](https://p5js.org/reference/#/p5/colorMode). For this I copied the code I found in the p5.js reference. I stil don't understand the code fully but I did figure out how to change the scale from the default 100 to 400 - hint: you just change all the '100's to '400's. **The hearts** I stumbled upon when I was browsing through the references at p5.js. The [example](https://p5js.org/reference/#/p5/describe) was quit small so I had to combine math and a trial and error strategy to figure out how to make it bigger. It gets tricky when you have to use three different shapes to make one. Lastly, I made the card **interactive** by adding a [mouseIsPressed function](https://p5js.org/reference/#/p5/mouseIsPressed). By looking at the example I figured I had to write all the changing elements under both 'if' and 'else' for it to work. And it did! Yay!  

##### The why  
The bigger inspiration behind my project is the tradition of Valentin's Day that has become more and more influential in Denmark over the years. Not because I support the tradition as it is today or the consumerism behind it. But it did make me think of the universal theme love. Because of my lacking programming experience - and hence my struggle to make something pleasing to the eye - I choose to go big with the symbolism. **The rainbow background** is in honor of the LGBTQ+ movement and the notion that all love is equal. **The text** is an invitation to act (perhaps on more levels?) and **the black hearts** and **text** that get colored when the mouse is pressed is a story about love that withers and looses its colors if we do not actively do something to keep it alive. So send the card to someone who needs a hint! ..or just someone who like rainbows and clicking on things. 

#### How would I describe my first independent coding experince? 
This project has been my first serious experience with coding. In the beginning I had to come to terms with the rigidty of it but you definitly learn how to "talk" to the computer more and more as you practice. At the end of this week I started to really appreciate coding as a tool to create and express myself. I think I will only grow fonder for coding as I learn more and my coding (hopefully) gets smoother. 

#### How is the coding process different from, or similiar to, reading and writing text? 
Is is similiar in the sense that you have to pratice it to get good at it - and stay good. I also understand the parallel being drawn to syntax in normal languages, but I would say that normal languages are more forgiving. Human try to - and often succeed at - trying to understand people how doesn't speak a language perfectly. This room for mistakes or interpretation does not exit in programming languages: either the computer understand you completely or not at all. 

#### What does code and programming mean to me, and how does the assigned reading help me to further reflect on these terms?
At this point code and programming is still a foreign language to me but a diffinitly have a better grasp of the rules and way of thinking already. The assigned reading gives me a broader perspective on coding and programming and helps me to think about it in other ways than just remembering the {}'s and ;'s. **Annette Vee's (2017)** notion of coding as a modern literacy was a very interesting angle. 

> “Seeing programming in light of the historical, social, and conceptual contexts of literacy helps us to understand computer programming as an important phenomenon of communication, not simply as another new skill or technology.”

### References 
1. [The Rainbow Gradient Background - p5.js reference](https://p5js.org/reference/#/p5/colorMode)
2. [The Hearts - p5.js reference]()
3. [mouseIsPressed - p5.js reference](https://p5js.org/reference/#/p5/mouseIsPressed)
4. [The Vee Quote - AP Book](https://aesthetic-programming.net/pages/1-getting-started.html#minix-runme-and-readme-28833)
