class Grass {
  constructor(_x, _bottom, _x2, _top) {
    this.speed = random(0.3, 0.7);
    this.x = _x;
    this.x2 = _x2
    this.bottom = _bottom;
    this.top = _top;
  }

  move() {
    if (this.top < height/2) {
      this.top = height;
    } else {
      this.top -= this.speed;
    }
  }

  show() {
    if (this.top < height*0.55) {
      stroke(96, 196, 82);
    } else {
      stroke(68, 157, 60)
    }
    strokeWeight(2);
    line(this.x, this.bottom, this.x2, this.top);
  }
}
