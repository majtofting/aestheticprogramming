## MiniX 7 Submission

Project this week: Revisit the past

> “The obstacles of your past can become the gateways that lead to new beginnings.” - Ralph Blum

#### RunMe and souce code

1.  [RUN](https://majtofting.gitlab.io/aestheticprogramming/miniX7/) the program

2.  [READ](https://gitlab.com/majtofting/aestheticprogramming/-/blob/main/miniX7/bricks.js) code for main sketch

3. [READ](https://gitlab.com/majtofting/aestheticprogramming/-/blob/main/miniX7/grass.js) code for class sketch

#### Which miniX have I reworked?
I choose to rework my [miniX5](https://gitlab.com/majtofting/aestheticprogramming/-/tree/main/miniX5) which I made in the week with the theme *Auto-generator*.

#### What have I changed and why?
In regards to changes, I have mainly focused on one element in the previous program that I didn't have the time or skills to make the way I initially had imagined. Fortunately, last week's introduction to object oriented programming gave me some ideas on how make alter the program to do what I originally wanted it to.

The thing I changed is how the "grass" moves. From the beginning I wanted to program each straw to "grow" at a different, random speed whilst using a for-loop, so that I wouldn't have to hard code each single straw. As evident in the picture below my first attempt resulted in grass straws without much difference in height/growth speed.

![Picture of before-and-after](/miniX7/before_after.png)

To make the change I decided to use OOP and create a `class Grass` and thereby make each straw to an object which I then called in an **for-loop** in draw by using an **array**.

<img src = 'bricks_1.png' width = 600> 

Another change I made was to make the grass change color to become lighter when the grass reaches full height - which I set to mid-height - both by using a conditional statement. After reaching the defined full height, a new straw starts to "grow" from the ground in the original color. I did this to magnify the effect of having a "auto-generative" program that continues to run and change over time no matter for how it runs.

<img src = 'bricks_2.png' width = 600> 

#### What have I learnt in this miniX?
This week I learnt more about OOP and feel like I understand the concepts a little better. For instance, I used the method of creating parameters within the `constructor` so that I could define these parameters later in setup. I did had some trouble with making the combination of objects, arrays and for-loops function which motivated me to ask for help and search for solutions. I therefore learnt some good lessons about this combination.

#### What is the relation between aesthetic programming and digital culture?

As Soon and Cox notes in the preface of their book, *Aesthetic Programming* is about the wider social, cultural and political implications of programming and computation. Instead of just investigating the practical aspect of how to *code* the book also considers the mentioned dimensions to bring light to programming as a tool to *think* and *act* (preferably critically). By drawing on the field of Software Studies, the approach seeks to expand current limitation within programming fields where the focus often is solely on learning the 'how to'.

In regards to digital culture, this approach underlines the importance of seeing programming in relation to the culture it made in and for. As software comes to influence society more and more it is crucial that we learn to understand the *political aesthetics* of data, algorithms, processing, object abstraction and so on.

> "Political aesthetics refers back to the critical theory of the Frankfurt School, particularly to the ideas of Theodor Adorno and Walter Benjamin, that enforce the concept that cultural production — which would now naturally include programming — must be seen in a social context. Understood in this way programming becomes a kind of “force-field” with which to understand material conditions and social contradictions, just as the interpretation of art once operated “as a kind of code language for processes taking place within society.”" (2)

In this way we will better be able to make programming - and the product that it created by it - accessible to more people (no matter class, gender, sexuality, ethnicity etc.) and thereby challenging existing power structures.  

##### How does my work demonstrate the perspective of aesthetic programming?

First of all, the work is *open source*, meaning that the work an source code is made accessible for other here at GitLab. In this way I am 'giving back' to the [p5.js community](https://p5js.org/community/) which is an essential part of aesthetic programming. Secondly, when conceptualizing my idea and creating the code, I have learnt throughout this course to have many more perspectives in mind besides just making a functional program. I try to be aware of the choices I make and my own biases so that my program doesn't unintentionally exclude someone or contribute to cultural and social structures that I do not agree with (i.e. reenforcing normative gender stereotypes). 

##### Sources
1. Daniel Shiffman, “Code! Programming with p5.js,” The Coding Train (video [6.3](https://www.youtube.com/watch?v=rHiSsgFRgx4) and [7.3](https://www.youtube.com/watch?v=fBqaA7zRO58&t=600s))
2. ["Preface"](https://aesthetic-programming.net/pages/preface.html)in *Aesthetic Programming* (2020)  
