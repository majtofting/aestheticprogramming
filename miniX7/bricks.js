
let x = 0;
let y = 0;
let spacing = 20;
let color1, color2, color3;
let grass = [];

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(226, 213, 196);
  frameRate(15);
  //text: Pink Floyd reference
  textSize(24);
  textFont('Georgia')
  textAlign(CENTER);
  text("All in all it's just...", width/4, height/3);
  text("... another brick in the wall", width-(width/4), height-height/4);
  for(let i = 0; i < width; i++) {
    grass[i] = new Grass(i, height, i, height-10);
  }
}

function draw() {
  drawBricks();
  drawGround();
  showGrass();
}

function showGrass() {
  for(let i = 0; i < grass.length; i+=3) {
    grass[i].move();
    grass[i].show();
  }
}

function drawBricks(){
  stroke(195, 183, 167);
  strokeWeight(3);
  color1 = color(140, 67, 45);
  color2 = color(177, 99, 63);
  color3 = color(87, 72, 71);
  if (random(4) > 2) {
    //brick color1
    fill(color1);
    rect(x, y, spacing*4, spacing);
  } else if (random(4) > 1) {
    //brick color2
    fill(color2);
    rect(x, y, spacing*4, spacing);
  } else if (random(4) > 0) {
    //brick color3
    fill(color3);
    rect(x, y, spacing*4, spacing);
  }
  x+=spacing*4; //draw new brick at end of brick prior brick
  if (x >= width) { //if bricks have filled out window vertically
    startPoint =  x - width //define starting point for bricks
     startPoint -- //decrement starting point
    x = 0 - startPoint*random(2, 3); //create random starting point for next line of bricks
    y += spacing;
  }
  if (y > height) { //if bricks have filled out window horisontally --> reset x and y
    x = 0;
    y = 0;
  }
}

function drawGround() {
  //ground
  noStroke();
  fill(68, 157, 60);
  rect(0, height-10, width, 10);
}
