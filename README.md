# AestheticProgramming

Here you can find my weekly miniX projects for the course Aesthetic Programming 2022 at Aarhus University. 

##### 1. [miniX 1: RunMe and ReadMe](https://gitlab.com/majtofting/aestheticprogramming/-/tree/main/miniX1)  

##### 2. [miniX 2: Emojis](https://gitlab.com/majtofting/aestheticprogramming/-/tree/main/miniX2) 

##### 3. [miniX 3: Throbber](https://gitlab.com/majtofting/aestheticprogramming/-/tree/main/miniX3)

##### 4. [miniX 4: Capture All](https://gitlab.com/majtofting/aestheticprogramming/-/tree/main/miniX4)

##### 5. [miniX 5: Auto Generator](https://gitlab.com/majtofting/aestheticprogramming/-/tree/main/miniX5)

##### 6. [miniX 6: Games with Objects](https://gitlab.com/majtofting/aestheticprogramming/-/tree/main/miniX6)

##### 7. [miniX 7: Revisit the Past](https://gitlab.com/majtofting/aestheticprogramming/-/tree/main/miniX7)

##### 8. [miniX 8: E-lit](https://gitlab.com/majtofting/aestheticprogramming/-/tree/main/miniX8)

##### 9. [miniX 9: Flowcharts](https://gitlab.com/majtofting/aestheticprogramming/-/tree/main/miniX9)

##### 10. [miniX 10: Machine Unlearning](https://gitlab.com/majtofting/aestheticprogramming/-/tree/main/miniX10)

Enjoy! 

![Monkey at computer](/images/gitlap_forside.gif "Footage of my coding process")
