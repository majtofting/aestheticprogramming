class Turtle {
  constructor() {
    this.size = {w: 158, h: 100};
    this.pos = {x: width / 2, y: height - this.size.h/2};
 }
  show() {
    imageMode(CENTER);
    image(turtleImg, this.pos.x, this.pos.y, this.size.w, this.size.h);
  }
  // intersects(other) {
  //   let d = dist(this.pos.x, this.pos.y, other.pos.x, other.pos.y);
  //   if (d < this.size/2 + other[i].size/2) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
}

class Bag {
  constructor() {
    this.speed = (random(0.5, 4));
    this.size = floor(random(40, 70));
    this.pos = new createVector(width + 5, random(50, height-120));
  }
  move() {
    this.pos.x -= this.speed;
  }
  show() {
    imageMode(CENTER);
    image(bagImg, this.pos.x, this.pos.y, this.size, this.size);
  }
}

class Bottle {
  constructor() {
    this.speed = (random(0.5, 4));
    this.size = floor(random(40, 60));
    this.pos = new createVector(random(width+10, width+100), random(50, height-120));
  }
  move() {
    this.pos.x -= this.speed;
  }
  show() {
    imageMode(CENTER);
    image(bottleImg, this.pos.x, this.pos.y, this.size*0.6, this.size);
  }
}

class Cig {
  constructor() {
    this.speed = (random(2, 4));
    this.size = {w: 24, h: 10};
    this.pos = new createVector(width + 40, random(50, height-120));
  }
  move() {
    this.pos.x -= this.speed;
  }
  show() {
    imageMode(CENTER);
    image(cigImg, this.pos.x, this.pos.y, this.size.w, this.size.h);
  }
}

class Algae {
  constructor() {
    this.size = floor(random(30, 60));
    this.x = random(this.size, width-this.size);
    this.y = random(this.size, height-this.size);
  }
  show() {
    imageMode(CENTER);
    image(algaeImg, this.x, this.y, this.size*1.2, this.size)
  }
}
