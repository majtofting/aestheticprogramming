function preload(){
  bagImg = loadImage('bagImage.png');
  turtleImg = loadImage('turtleImg.png');
  bottleImg = loadImage('bottleImage.png');
  cigImg = loadImage('cigaretteImage.png');
  algaeImg = loadImage('algaeImage.png');
  gameOver = loadImage('gameOver.jpg');
}

let min_bag = 5; //minimum bags on the screen
let min_bottle = 5;
let min_cig = 7;
let min_algae = 5;
let bag = [];
let bottle = [];
let cig = [];
let algae = [];
let turtle;
let score = 0, lifes = 3;
let bottom;

function setup() {
  createCanvas(1200, 500);
  bottom = height;
  turtle = new Turtle();
}

function draw() {
  background(0, 151, 230);
  checkBagNum();
  checkBottleNum();
  checkCigNum();
  checkAlgaeNum();
  showBag();
  showBottle();
  showCig();
  showAlgae();
  checkRemoveBag();
  checkRemoveBottle();
  checkRemoveCig();
  turtle.show();
  checkEatingAlgae();
  displayScore();
  checkResult();
  encourage();
}

function checkBagNum() {
  if (bag.length < min_bag) {
    bag.push(new Bag());
  }
}

function checkBottleNum() {
  if (bottle.length < min_bottle) {
    bottle.push(new Bottle());
  }
}

function checkCigNum() {
  if (cig.length < min_cig) {
    cig.push(new Cig());
  }
}

function checkAlgaeNum() {
  if (algae.length < min_algae) {
    algae.push(new Algae());
  }
}

function showBag() {
  for (let i = 0; i < bag.length; i++) {
    bag[i].move();
    bag[i].show();
  }
}

function showBottle() {
  for (let i = 0; i < bottle.length; i++) {
    bottle[i].move();
    bottle[i].show();
  }
}

function showCig() {
  for (let i = 0; i < cig.length; i++) {
    cig[i].move();
    cig[i].show();
  }
}

function showAlgae() {
  for (let i = 0; i < algae.length; i++) {
    algae[i].show();
  }
}

function checkRemoveBag() {
  for (let i = 0; i < bag.length; i++) {
    let d = int(dist(turtle.pos.x, turtle.pos.y, bag[i].pos.x, bag[i].pos.y));
    if (d < turtle.size.h/2) {
      bag.splice(i, 1);
      lifes--;
    } else if (bag[i].pos.x < 0) {
       bag.splice(i,1);
     }
   }
 }

 function checkRemoveBottle() {
   for (let i = 0; i < bottle.length; i++) {
     let d = int(dist(turtle.pos.x, turtle.pos.y, bottle[i].pos.x, bottle[i].pos.y));
     if (d < turtle.size.h/2) {
       bottle.splice(i, 1);
       lifes--;
   } else if (bottle[i].pos.x < 0) {
        bottle.splice(i,1);
      }
    }
  }

  function checkRemoveCig() {
    for (let i = 0; i < cig.length; i++) {
      let d = int(dist(turtle.pos.x, turtle.pos.y, cig[i].pos.x, cig[i].pos.y));
      if (d < turtle.size.h/2) {
        cig.splice(i, 1);
        lifes--;
    } else if (cig[i].pos.x < 0) {
         cig.splice(i,1);
       }
     }
   }

function checkEatingAlgae() {
  for (let i = 0; i < algae.length; i++) {
    let d = int(dist(turtle.pos.x, turtle.pos.y, algae[i].x, algae[i].y));
  if (d < turtle.size.h/2) {
    algae.splice(i, 1);
    score++;
   }
  }
}

function displayScore() {
  fill(255);
  textSize(20);
  text('Algae eaten: '+score, 10, height-50);
  text('Lifes left: '+lifes, 10, height-25);
}

function checkResult() {
  if (lifes == 0) {
    imageMode(CENTER);
    image(gameOver, width/2, height/2, 400, 240);
    fill(255);
    textSize(20);
    textAlign(CENTER);
    text('The sea turtle ate too much trash...', width/2, 110);
    text('GAME OVER', width/2, 400)
    noLoop();
  }
}

function encourage() {
  if (score >= 50 && score < 53) {
    fill(255);
    textsize(20);
    text('Sea turtle loves you <3', width/3, 30)
  } else if (score >= 40 && score < 43) {
    fill(255);
    textSize(20);
    text("Let's have a shellebration", width/3, 30);
  } else if (score >= 30 && score < 33) {
    fill(255);
    textSize(20);
    text("I'm in turtle awe of your skills!", width/3, 30);
  } else if (score >= 20 && score < 23) {
    fill(255);
    textSize(20);
    text('"This is turtley amazing!"', width/3, 30);
  } else if (score >= 10 && score < 13) {
    fill(255);
    textSize(20);
    text('"Algae... yum!"', width/3, 30);
  }
}

 function keyPressed() {
   if (keyCode === UP_ARROW) {
     turtle.pos.y -= 50;
   } else if (keyCode === DOWN_ARROW) {
     turtle.pos.y += 50;
   } else if (keyCode === LEFT_ARROW) {
     turtle.pos.x -= 50;
   } else if (keyCode === RIGHT_ARROW) {
     turtle.pos.x += 50;
   }
   if (turtle.pos.y > bottom) {
     turtle.pos.y = bottom;
   } else if (turtle.pos.y < 0) {
     turtle.pos.y = 0;
   }
   if (turtle.pos.x < 0) {
     turtle.pos.x = 0;
   } else if (turtle.pos.x > width) {
      turtle.pos.x =  width;
   }

   return false;
 }
