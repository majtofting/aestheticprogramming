## MiniX6 Submission

Project this week: Games with Objects

> "Therein lies part of the motivation for this chapter, to understand that objects are designed with certain assumptions, biases and worldviews, and to make better object abstractions and ones with a clearer sense of purpose." - Soon & Cox, *Aesthetic Programming (2020)* 

#### How to see my game 
1. Run the [program](https://majtofting.gitlab.io/aestheticprogramming/miniX6/)

2. Read the [code](https://gitlab.com/majtofting/aestheticprogramming/-/blob/main/miniX6/game.js)

#### How does the game work? 
The game consists of five different **object classes** where one is the character (a sea turtle), one is the token of the game (algae), and the three remaining are the obstacles (plastic bag, plastic bottle, cigarette butts). The goal of the game is to navigate the turtle around to eat the algae while dodging the trash. For every 10th algae eaten the program prints an encouraging sentence on top of the screen like *"This is turtley amazing"*. 

![Picture of the game](/miniX6/feed_the_turtle.png)

As noted in the bottom left corner, the game starts with 3 lifes. You lose 1 life everytime the turtle eats trash instead of algea. When all 3 lifes are lost, the 'game over'-screen below appears.

![Picture of the game](/miniX6/game_over.png)

To make sure that new algae and trash is continously spawned, I used declared a variabel to define a minimum number of each object and then created a conditionel statement to check if the number got below minimum - and in that case `push(new Object())` to create a new object. To remove the algae and the trash I used `object.splice` in combination with for-loops and *if* and *else if* statements to remove objects if the `dist` between turtle and the algae/trash became smaller than half the size of the turtle. The trash is also removed when `trash.pos.x < 0`. I used `keyPressed` to create a function in the main sketch to make the turtle move by pressing `UP/DOWN/LEFT/RIGHT_ARROW`. Because I made a class for the turtle, I was able to 'pull' the turtle's position from there, e.g.: ´turtle.pos.y -= 50´. 

#### How I programmed the objects and their related attributes and methods 
As we read in [this week's chapter](https://aesthetic-programming.net/pages/6-object-abstraction.html) the creation of classes and objects can be divided into 5 steps. I will now demonstate these steps using my class `Bag` as example. 

1. step: I **named** my class 'Bag' --> `class Bag` 
2. step: I defined the **arributes/proprties** of the bag in `constructor()` --> `this.size = ()`, `this.pos = ()`, `this.speed = ()` 
3. step: I defined the **methods/behaviors** of the bag --> `show()`, `move()`
4. step: I **created** a object in the main sketch --> `push(new Bag())`
5. step: I added **trigger points & logic** to the object --> `bag.splice`, `bag.push`

#### What are the characteristics of objects oriented programming and the wider implications of abstraction? 
Matthew Fuller and Andrew Goffrey defines object orientation as "an approach to computing that views programs in terms of the interactions between programmatically defined objects – computational objects - rather than as an organized sequence of tasks embodied in a strictly defined ordering of routines and sub-routines" and objects as "groupings of data and the methods that can be executed on that data, or *stateful abstractions*". But what is meant by these *abstractions*? 

In my understanding, it refers to how the construction of objects always includes making choices about what to include and what to leave out. This not only applies to the attributes, methods, and logic, but also to the role you make the object play in your program. An example of this is the a classic console game like Super Mario where the plot often rewolves around how the normatively masculine Mario-character, as an active actant, needs to save the passive, normatively feminine princess, Peach. This lead to a question of agency which is also discussed by Soon & Cox (2020): 

> "Matthew Fuller and Andrew Goffey suggest that this object-oriented modeling of the world is a social-technical practice, “compressing and abstracting relations operative at different scales of reality, composing new forms of agency."" (1)

As I understand it, the agency refered to in the quote above can also refer to the relation between human actors and the computational. The "object-oriented modeling of the world" impacts how we understand (or think we understand) how the computer functions and this abstraction can be seen as distorting and hiding the actual functionality. But perhaps this isn't solely a bad thing? The computer is cabable of doing things that the human mind simple can't comprehend and precisely beacuse this it perhaps makes sense that we need alternative ways to think about computing and programming in order to get a grasp of it. Personally, I find OOP to be a more manageble way to maintain an overview whilst programming with multible elements that needs to *behave* differently - enough it entails making simplified versions of actual object. Just as prejucedes is an inherent ability in the human mind that saves us cognitive power in everyday life, approaches like OOP is probably neccessary to bring "understanding" of computation down to a level where our cognition can keep up. 

Untheless, it is important to have these aspects in mind while operating with OOP and computation in general. 

#### My game in a wider cultural context 
Overall, the game has a theme that deals with plastic and trash in the ocean and how this affects wild life living there, i.e. the sea turtle. The game encourages the player to think of trash in the ocean as a 'enemy' that threatens the life of the turtle. In regards to OOP my choices of what to include in the different classes affects the message of the game, the experience of playing it and the story it tells about the objects and their interrelationships. For instance, the sea turtle has little defence in regards to dodging the trash: it is only able to swim away. Furthermore, my program sends the message that the turtle *will* eat trash just because it's there and that the problem therefore lies in the turtle not being able to get around due to all the trash. While this may be true some places, another important issue lies more in the turtle's missing ability to distinguish between what is edible and what is not. I could mention more examples like this but the main point is that my biases and worldview have influenced which attributes and methods I choose to add and leave out.

Another thing worth mentioning is that at start the game doesn't offer any instruction on how to play it. This tells something about my own bias and implied knowledge of what options a game usually provides in term of interactivity (e.i. moving a character by using the arrow keys). Further discussion could be about whether this makes the games less inclusive - and in that case, who it excludes? 

##### Resources 
1. Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
2. Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). 
3. Daniel Shiffman, “Code! Programming with p5.js,” The Coding Train, https://www.youtube.com/watch?v=8j0UDiN7my4&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA.
