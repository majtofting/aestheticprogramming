let radio1, radio2, radio3, radio4, radio5, radio6, radio7, radio8, radio9, radio10;
let x = 40;
let y = 80;
let on = false;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255, 235, 205);
  //header
  push();
  textSize(32);
  text('Who Are You?', 40, 60);
  pop();
  //radio1
  radio1 = createRadio();
  radio1.option('Cats');
  radio1.option('Dogs');
  radio1.style('width', '60 px');
  radio1.position(x, y);
  radio1.attribute('name','first');
  //radio2
  radio2 = createRadio();
  radio2.option('Summer');
  radio2.option('Winter');
  radio2.style('width', '60 px');
  radio2.position(x, y+30);
  radio2.attribute('name', 'second');
  //radio3
  radio3 = createRadio();
  radio3.option('Car');
  radio3.option('Bike');
  radio3.style('width', '60 px');
  radio3.position(x, y+60);
  radio3.attribute('name', 'third');
  //radio4
  radio4 = createRadio();
  radio4.option('Coffee');
  radio4.option('Tea');
  radio4.style('width', '60 px');
  radio4.position(x, y+90);
  radio4.attribute('name', 'forth');
  //radio5
  radio5 = createRadio();
  radio5.option('Cooking');
  radio5.option('Takeaway');
  radio5.style('width', '60 px');
  radio5.position(x, y+120);
  radio5.attribute('name', 'fifth');
  //radio6
  radio6 = createRadio();
  radio6.option('Roomates');
  radio6.option('Live alone');
  radio6.style('width', '60 px');
  radio6.position(x, y+150);
  radio6.attribute('name', 'sixth');
  //radio7
  radio7 = createRadio();
  radio7.option('Save');
  radio7.option('Spend');
  radio7.style('width', '60 px');
  radio7.position(x, y+180);
  radio7.attribute('name', 'seventh');
  //radio8
  radio8 = createRadio();
  radio8.option('Fiction');
  radio8.option('Nonfiction');
  radio8.style('width', '60 px');
  radio8.position(x, y+210);
  radio8.attribute('name', 'eighth');
  //radio9
  radio9 = createRadio();
  radio9.option('Netflix');
  radio9.option('Books');
  radio9.style('width', '60 px');
  radio9.position(x, y+240);
  radio9.attribute('name', 'ninth');
  //radio10
  radio10 = createRadio();
  radio10.option('Ambition');
  radio10.option('Comfort');
  radio10.style('width', '60 px');
  radio10.position(x, y+270);
  radio10.attribute('name', 'tenth');
  //radio11
  radio11 = createRadio();
  radio11.option('Reality shows');
  radio11.option('Documentaries');
  radio11.style('width', '60 px');
  radio11.position(x+200, y);
  radio11.attribute('name','eleventh');
  //radio12
  radio12 = createRadio();
  radio12.option('City');
  radio12.option('Countryside');
  radio12.style('width', '60 px');
  radio12.position(x+200, y+30);
  radio12.attribute('name','twelfth');
  //radio13
  radio13 = createRadio();
  radio13.option('Welfare');
  radio13.option('Climate');
  radio13.style('width', '60 px');
  radio13.position(x+200, y+60);
  radio13.attribute('name','thirteenth');
  //radio14
  radio14 = createRadio();
  radio14.option('Half full');
  radio14.option('Half empty');
  radio14.style('width', '60 px');
  radio14.position(x+200, y+90);
  radio14.attribute('name','fourteenth');
  //radio15
  radio15 = createRadio();
  radio15.option('Train');
  radio15.option('Airplane');
  radio15.style('width', '60 px');
  radio15.position(x+200, y+120);
  radio15.attribute('name','fifteenth');
  //radio16
  radio16 = createRadio();
  radio16.option('Meat');
  radio16.option('Vegetables');
  radio16.style('width', '60 px');
  radio16.position(x+200, y+150);
  radio16.attribute('name','sixteenth');
  //radio17
  radio17 = createRadio();
  radio17.option('High taxes');
  radio17.option('Low taxes');
  radio17.style('width', '60 px');
  radio17.position(x+200, y+180);
  radio17.attribute('name','seventeenth');
  //radio18
  radio18 = createRadio();
  radio18.option('Skiing');
  radio18.option('Sunbathing');
  radio18.style('width', '60 px');
  radio18.position(x+200, y+210);
  radio18.attribute('name','eighteenth');
  //radio19
  radio19 = createRadio();
  radio19.option('Pancake');
  radio19.option('Waffles');
  radio19.style('width', '60 px');
  radio19.position(x+200, y+240);
  radio19.attribute('name','ninteenth');
  //radio20
  radio20 = createRadio();
  radio20.option('Consent');
  radio20.option('Agree');
  radio20.style('width', '60 px');
  radio20.position(x+200, y+270);
  radio20.attribute('name','twentieth');
}
  function draw() {
  //create endnote
  //define "on"
  if(on){
    push();
    translate(width/2, 390);
    fill(222, 184, 135);
    textSize(16);
    textAlign(CENTER);
    text('Thank you for your data. It was nice getting to know you a little better.', 0, 0);

    pop();
  }
}
  function mouseClicked() {
    if(mouseX > 240 && mouseX < 330 && mouseY > 350 && mouseY < 360) {
      on = !on
    }
  }

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
