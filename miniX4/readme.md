## MiniX 4 Submission

Project this week: Capture All

>“Are there still modes of being that resist the imperative of digital capitalism to CAPTURE ALL or is there no option but to play along? If so, are there artistic strategies and speculative approaches that do not play this game of quantification by the numbers? What are the […] gaps of relentless quantification and gamification that can be exploited in order to carve out new ways of living?” (Transmediale 2015 - call for works) (1)

#### How to see my program

1.  [RUN](https://editor.p5js.org/maj_tofting/full/gjC9aV673) my program (*links to p5.js Editor as the program doesn't execute properly in Atom/GitLap*)

2.  [READ](https://gitlab.com/majtofting/aestheticprogramming/-/blob/main/miniX4/button.js) the code

**You can see the actual runMe [here](https://majtofting.gitlab.io/aestheticprogramming/miniX4/) (doesn't work properly)*

#### About the program

##### 1. Submission to Transmediale
###### Title: *Who Are You?* (2022)
This work explores the relation between data and identity/integrity and serves a critique of the ways in which we get lured into giving away our data in the form of personal preferences - often lead by creating the illusion that however is behind is actually interested in us and to cover up the fact that nothing on the internet is free anymore. Your are paying with our data.

##### 2. Description
My program consists mostly of radio buttons - 20 of them to be exact. Each radio button gives the user two options that are suppose to be understood as a 'this or that' questions. I made the radio buttons by creating 20 individual radio button with `createRadio`. Furthermore I used `radio.option` to create the two options for each and `radio.position` to place them apart from each other. I then found out that I also had to use `radio.attribute` to give each radio button its own name. By doing this the computer knows that each radio button is separate from the rest and thereby gives it its own 'blue dot' (see picture below). If the name-attribute isn't used the computer thinks that all 40 options are under the same 'radio'.

![Picture of program](/miniX4/button.png "Screenshot of the program after interacting")

By the 20th radio button the options are 'consent' or 'agree'. By choosing either one of these option a text will appear on the screen saying: *__'Thank you for your data. It was nice getting to know you a little better'__*. This function I made by creation a `mouseClicked` function that makes the computer draw the text when the mouse is clicked within a limited area (in this program, set to the area where the last two options are placed).

##### 3. Thoughts behind
In relation to the "Capture All" theme the intention with my program was to create a critical design that lures the user to give away information that can contribute to making a personality analysis that can be sold to companies etc. who might profit from the data. On the surface the program appears 'friendly' as it invites you to tell about yourself. This friendliness continues in the text at the end where it
imitates courtesies a new person you just met might say (the same kind of apparent courtesy as when Facebook expands it's gender option under the appearance of being more inclusive). The sinister side of it is just that the program (and maybe Facebook too?) is only out to get your data.

The inspiration to the program comes from several places. Firstly I got the idea to the "Who Are You?" theme from Ulisis and Couldry's (2019) text *Datafication* (2) where they describe how a possible concern in regards to datafication is the way in which we become more and more dependent on data measures to tell us who we are.   

> (...) "Similar concerns have been
expressed in terms of attempts by marketers and others to influence behaviour through data
analytics (cf. Rouvroy, 2015, on “data behaviorism”). This line of critique argues that we are,
through datafication, becoming dependent on (external, privatised) data measurements to tell
us who we are, what we are feeling, and what we should be doing, which challenges our basic
conception of human agency and knowledge." (Ulisis and Couldry, 2019)

I wanted to try and reverse this be letting the users tell who they are themselves.

That being said, the program is also a comment on the binary quality of buttons and - in the case of my program - the limitations of having only two options to choose from in a 'this or that' format. With radio buttons you are forced to choose only one option and these removes the possible to create a more nuanced and representing picture of one's preferences.

I chose to use buttons because of Soon and Cox's (2020) notion about the *seductiveness* of buttons (3). Persuasive elements are useful when you want to lure the data out of someone.

Lastly, the inspiration to the last option in the program where it is only possible to chose between 'Consent' or 'Agree' comes from the cookies popups that you met on almost every website you visit. It is meant as a comment on how many sites try to discourage users from not accepting both necessary *and* unnecessary cookies.  

##### References
1. [Transmediale 2015 - call for works](https://archive.transmediale.de/content/call-for-works-2015)
2. [Datafication (Ulisis & Couldry, 2019)](https://policyreview.info/concepts/datafication)
3. [Data capture (Soon & Cox, 2020)](https://aesthetic-programming.net/pages/4-data-capture.html#minix-capture-all-15243)
